<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

 <!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ログイン画面</title>
	<link rel="stylesheet"
		href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
		integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
		crossorigin="anonymous">

	<link href="css/original/original.css" rel="stylesheet">
</head>
<body>
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
    <div class="container">
        <h1>ログイン画面</h1>
        <div class="row">
            <div class="col"></div>
            <div class="col-3">
                <form action="LoginServlet" class="form-signin" method="POST">
                    <div class="form-group">
                        <label for="loginId">ログインID　：</label>
                        <input type="text" name="loginId" id="loginId" class="form-control" placeholder="ログインIDを入力してください">
                    </div>
                    <div class="form-group">
                        <label for="password">パスワード　：</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="パスワードを入力してください">
                    </div>
                    <button type="submit" class="btn btn-secondary login">ログイン</button>
                </form>
            </div>
            <div class="col"></div>
        </div>
    </div>
</body>
</html>