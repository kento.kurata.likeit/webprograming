<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
	<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ユーザー新規登録画面</title>
	<link rel="stylesheet"
		href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
		integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
		crossorigin="anonymous">

	<link href="css/original/original.css" rel="stylesheet">
	</head>
	<body>
		<header>
			<nav class="navbar navbar-dark bg-dark">
				<div class="col-sm-4"></div>
				<div class="col-sm-4 ">
					<p>${userInfo.name}さん</p>
				</div>
				<div class="col-sm-4 align-right">
					<a class="logout" href="LogoutServlet">ログアウト</a>
				</div>
			</nav>
		</header>
	    <div class="container">
	    	<c:if test="${errMsg1 != null}" >
				<div class="alert alert-danger" role="alert">${errMsg1}</div>
			</c:if>
			<c:if test="${errMsg2 != null}" >
				<div class="alert alert-danger" role="alert">${errMsg2}</div>
			</c:if>
			<c:if test="${errMsg3 != null}" >
				<div class="alert alert-danger" role="alert">${errMsg3}</div>
			</c:if>
	        <h1>ユーザー新規登録</h1>
	        <div class="row">
	            <div class="col"></div>
	            <div class="col-6">
	                <form action="UserCreateServlet" class="form-signin" method="POST">
	                    <div class="form-group">
	                        <label for="loginId">ログインID</label>
	                        <input type="text" name="loginId" id="loginId" class="form-control" placeholder="ログインIDを入力してください">
	                    </div>
	                    <div class="form-group">
	                        <label for="password">パスワード</label>
	                        <input type="password" name="password" id="password" class="form-control" placeholder="パスワードを入力してください">
	                    </div>
	                    <div class="form-group">
	                        <label for="password">パスワード(確認)</label>
	                        <input type="password" name="kakuninpassword" id="kakuninpassword" class="form-control" placeholder="パスワードを入力してください">
	                    </div>
	                    <div class="form-group">
	                        <label for="userName">ユーザー名</label>
	                        <input type="text" name="userName" id="userName" class="form-control" placeholder="ユーザー名を入力してください">
	                    </div>
	                    <div class="form-group">
	                        <label for="birthDate">生年月日</label>
	                        <input type="date" name="birthDate" id="birthDate" class="form-control">
	                    </div>
	                    <input type= "submit" value="登録">
	                    <p><a class="back" href="UserLIstServlet">戻る</a></p>
	                </form>
	            </div>
	            <div class="col"></div>
	        </div>
	    </div>
	</body>
</html>