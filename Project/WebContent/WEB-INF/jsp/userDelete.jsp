<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ユーザー情報削除画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">

<link href="css/original/original.css" rel="stylesheet">
</head>
<body>
	<header>
		<nav class="navbar navbar-dark bg-dark">
			<div class="col-sm-4"></div>
			<div class="col-sm-4 ">
				<p>${userInfo.name}さん</p>
			</div>
			<div class="col-sm-4 align-right">
				<a class="logout" href="LogoutServlet">ログアウト</a>
			</div>
		</nav>
	</header>
	<div class="container">
		<h1>ユーザー削除確認</h1>
		<div class="row">
			<div class="col"></div>
			<div class="col-4">
				<p>
					ログインID: <span>${userDetail.loginId}</span>
				</p>
				<p class="bottom">を本当に削除してもよろしいでしょうか？</p>
			</div>
			<div class="col"></div>
		</div>
		<form action="UserDeleteServlet" method="POST">
			<input type="hidden" name="id" id="id" class="form-control" value="${userDetail.id}">
			<div class="container">
				<div class="row justify-content-md-center">
					<div class="col col-lg-2">
						<button type="button" onclick="history.back()"
							class="btn btn-primary">キャンセル</button>
					</div>
					<div class="col-md-auto"></div>
					<div class="col col-lg-2">
						<input class="btn btn-primary" type="submit" value="OK" name="OK">
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>