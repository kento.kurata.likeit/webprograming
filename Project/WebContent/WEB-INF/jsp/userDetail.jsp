<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ユーザー情報詳細参照</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">

<link href="css/original/original.css" rel="stylesheet">
</head>
<body>
    <header>
		<nav class="navbar navbar-dark bg-dark">
			<div class="col-sm-4"></div>
			<div class="col-sm-4 ">
				<p>${userInfo.name}さん</p>
			</div>
			<div class="col-sm-4 align-right">
				<a class="logout" href="LogoutServlet">ログアウト</a>
			</div>
		</nav>
	</header>
    <div class="container">
        <h1>ユーザー情報詳細参照</h1>
        <div class="row">
            <div class="col"></div>
            <div class="col-6">
                <div class="list-name">
                    <table>
                        <tr>
                            <th>ログインID</th>
                            <td>${userDetail.loginId}</td>
                        </tr>
                        <tr>
                            <th>ユーザー名</th>
                            <td>${userDetail.name}</td>
                        </tr>
                        <tr>
                            <th>生年月日</th>
                            <td>${userDetail.birthDate}</td>
                        </tr>
                        <tr>
                            <th>登録日時</th>
                            <td>${userDetail.createDate}</td>
                        </tr>
                        <tr>
                            <th>更新日時</th>
                            <td>${userDetail.updateDate}</td>
                        </tr>
                    </table>
                </div>
                <p><a class="back" href="UserLIstServlet">戻る</a></p>
            </div>
            <div class="col"></div>
        </div>
    </div>
</body>
</html>