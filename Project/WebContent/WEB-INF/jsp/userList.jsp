<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ユーザー検索一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">

<link href="css/original/original.css" rel="stylesheet">
</head>
<body>
	<header>
		<nav class="navbar navbar-dark bg-dark">
			<div class="col-sm-4"></div>
			<div class="col-sm-4 ">
				<p>${userInfo.name}さん</p>
			</div>
			<div class="col-sm-4 align-right">
				<a class="logout" href="LogoutServlet">ログアウト</a>
			</div>
		</nav>
	</header>
	<div class="container">
		<h1>ユーザー検索</h1>
		<div class="row">
			<div class="col"></div>
			<div class="col-6">
				<p>
					<a href="UserCreateServlet" class="new-acount">新規登録</a>
				</p>
				<form action="UserLIstServlet" class="form-signin" method="POST">
					<div class="form-group">
						<label for="loginId">ログインID ：</label> <input type="text"
							name="loginId" id="loginId" class="form-control"
							placeholder="ログインIDを入力してください">
					</div>
					<div class="form-group">
						<label for="name">ユーザー名 ：</label> <input type="text"
							name="name" id="name" class="form-control"
							placeholder="ユーザー名を入力してください">
					</div>
					<div class="form-group">
						<label for="birthDate">生年月日 ：</label> <input type="date"
							name="birthDate1" id="birthDate1"> <span> ~ </span> <input
							type="date" name="birthDate2" id="birthDate2">
					</div>
					<p><input type="submit" class="btn btn-secondary search" value ="検索"></p>
				</form>
			</div>
			<div class="col"></div>
		</div>
		<hr>
		<table>
			<tr>
				<th>ログインID</th>
				<th>ユーザー名</th>
				<th>生年月日</th>
				<th></th>
			</tr>
				<tbody>
               	<c:choose>
               		<c:when test="${userFound == null}">
		                 <c:forEach var="user" items="${userList}" >
		                   <tr>
		                     <td>${user.loginId}</td>
		                     <td>${user.name}</td>
		                     <td>${user.birthDate}</td>
		                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
		                     <td>
		                       <a class="btn btn-primary btnn" href="UserDetailServlet?id=${user.id}">詳細</a>
		                       <c:if test="${userInfo.id == user.id || userInfo.id == 1 }">
		                       		<a class="btn btn-success btnn" href="UserUpdateServlet?id=${user.id}">更新</a>
		                       </c:if>
		                       <c:if test="${userInfo.id==1 }">
		                       		<a class="btn btn-danger btnn" href ="UserDeleteServlet?id=${user.id}">削除</a>
		                       </c:if>
		                     </td>
		                   </tr>
		                 </c:forEach>
		        	</c:when>
			        <c:when test="${userFound != null}">
			        		<c:forEach var="user" items="${userFound}" >
			                   <tr>
			                     <td>${user.loginId}</td>
			                     <td>${user.name}</td>
			                     <td>${user.birthDate}</td>
			                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
			                     <td>
			                       <a class="btn btn-primary btnn" href="UserDetailServlet?id=${user.id}">詳細</a>
			                       <c:if test="${userInfo.id == user.id || userInfo.id == 1 }">
			                       		<a class="btn btn-success btnn" href="UserUpdateServlet?id=${user.id}">更新</a>
			                       </c:if>
			                       <c:if test="${userInfo.id == 1}">
			                       		<a class="btn btn-danger btnn" href ="UserDeleteServlet?id=${user.id}">削除</a>
			                       </c:if>
			                     </td>
			                   </tr>
						  </c:forEach>
		        	</c:when>
				</c:choose>
               </tbody>
		</table>
	</div>
</body>
</html>