<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ユーザー情報更新画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
	integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
	crossorigin="anonymous">

<link href="css/original/original.css" rel="stylesheet">
</head>
<body>
	<header>
		<nav class="navbar navbar-dark bg-dark">
			<div class="col-sm-4"></div>
			<div class="col-sm-4 ">
				<p>${userInfo.name}さん</p>
			</div>
			<div class="col-sm-4 align-right">
				<a class="logout" href="LogoutServlet">ログアウト</a>
			</div>
		</nav>
	</header>
	<div class="container">
		<c:if test="${errMsg1 != null}">
			<div class="alert alert-danger" role="alert">${errMsg1}</div>
		</c:if>
		<c:if test="${errMsg2 != null}">
			<div class="alert alert-danger" role="alert">${errMsg2}</div>
		</c:if>
		<h1>ユーザー情報更新</h1>
		<div class="row">
			<div class="col"></div>
			<div class="col-6">
				<form action="UserUpdateServlet" class="form-signin" method="POST">
					<input type="hidden" name="id" id="id" value="${userDetail.id}">
					<table>
						<tr>
							<th>ログインID</th>
							<td>${userDetail.loginId}</td>
						</tr>
						<tr>
							<th><label for="password">パスワード</label></th>
							<td><input type="password" name="password" id="password"
								class="form-control" placeholder="パスワードを入力してください。"></td>
						</tr>
						<tr>
							<th><label for="password">パスワード(確認)</label></th>
							<td><input type="password" name="kakuninpassword"
								id="kakuninpassword" class="form-control"
								placeholder="パスワードを入力してください。"></td>
						</tr>
						<tr>
							<th><label for="userName">ユーザー名</label></th>
							<td><input type="text" name="userName" id="userName"
								class="form-control" value="${userDetail.name}"></td>
						</tr>
						<tr>
							<th><label for="birthDate">生年月日</label></th>
							<td><input type="date" name="birthDate" id="birthDate"
								class="form-control" value="${userDetail.birthDate}"></td>
						</tr>
					</table>
					<input type="submit" class="btn btn-secondary reg btnn" value="更新">
					<p>
						<a class="back" href="UserLIstServlet">戻る</a>
					</p>
				</form>
			</div>
			<div class="col"></div>
		</div>
	</div>
</body>
</html>