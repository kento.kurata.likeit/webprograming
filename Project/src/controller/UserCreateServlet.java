package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserCreateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {

		HttpSession session = request.getSession();
		User userInfo = (User)session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("LoginServlet");
		}else {
			//userCreat.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreat.jsp");
			dispatcher.forward(request, response);
		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password1 = request.getParameter("password");
		String password2 = request.getParameter("kakuninpassword");
		String name = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");

		// パスワード不一致の場合
		if(!(password1.equals(password2))) {
			request.setAttribute("errMsg1", "パスワードが不一致です。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreat.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// 何かしら空場合
		if(loginId.equals("")||
				password1.equals("")||
				password2.equals("")||
				name.equals("")||
				birthDate.equals("")) {
			request.setAttribute("errMsg2", "全ての入力欄を埋めてください。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreat.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//ログインIDが同じ場合
		UserDao userDao = new UserDao();
		User user = userDao.findByLoginId1(loginId);

		if(!(user == null)) {
			request.setAttribute("errMsg3", "既にログインIDが使用されています。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreat.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//暗号化
		UserDao userDao1 = new UserDao();
		String secret = userDao1.secret(password1);

		UserDao userDao2 = new UserDao();
		userDao2.userCreate(loginId, secret, name, birthDate);

		response.sendRedirect("UserLIstServlet");

	}


}
