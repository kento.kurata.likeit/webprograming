package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

		HttpSession session = request.getSession();
		User userInfo = (User)session.getAttribute("userInfo");

		if (userInfo == null) {
			response.sendRedirect("LoginServlet");
		}

		String id = request.getParameter("id");

		UserDao userDao = new UserDao();
		User user = userDao.ucerDetail(id);

		HttpSession session1 = request.getSession();
		session1.setAttribute("userDetail", user);

		//userList.jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String password1 = request.getParameter("password");
		String password2 = request.getParameter("kakuninpassword");
		String name = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");
		String id = request.getParameter("id");

		//パスワード不一致の場合
		if (!(password1.equals(password2))) {
			request.setAttribute("errMsg1", "パスワードが不一致です。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		//password以外の空欄がある場合
		// 何かしら空場合
		if (name.equals("") ||
				birthDate.equals("")) {
			request.setAttribute("errMsg2", "パスワード以外の空欄を埋めてください。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		UserDao userDao1 = new UserDao();
		String secret = userDao1.secret(password1);

		UserDao userDao2 = new UserDao();
		userDao2.userUpdate(id, secret, name, birthDate);

		response.sendRedirect("UserLIstServlet");

	}

}
