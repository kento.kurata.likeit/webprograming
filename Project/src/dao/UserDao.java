package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {
	//loginメソッド
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// 確認済みのSQL
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			int id = rs.getInt("id");
			return new User(loginIdData, nameData, id);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//全ユーザー表示メソッド
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// 確認済みのSQL
			String sql = "SELECT * FROM user where not id = 1";

			 // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()) {
            	int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");

                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//新規登録
	public void userCreate(String loginId, String password, String name, String birthDate) {
    	Connection conn = null;
        try {

            conn = DBManager.getConnection();

            String sql = "insert into user(login_id, password, name, birth_date, create_date, update_date) value(?, ? ,? ,? , NOW(), NOW())";

            PreparedStatement pStmt = conn.prepareStatement(sql);

            pStmt.setString(1, loginId);
            pStmt.setString(2,password);
            pStmt.setString(3, name);
            pStmt.setString(4, birthDate);

            pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
	//ログインの一致を確認するメソッド
	public User findByLoginId1(String loginId) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();
			String sql = "SELECT login_id FROM user WHERE login_id = ?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理
			String loginIdData = rs.getString("login_id");

			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
//参照メソッド
	public User ucerDetail(String id) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();
			String sql = "SELECT id,login_id, name, birth_date, create_date, update_date FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, id);

			ResultSet rs = pStmt.executeQuery();

			if(!rs.next()) {
				return null;
			}

			int id1 = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			User user = new User(id1, loginId, name, birthDate, createDate, updateDate);

			return user;

 		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	//更新メソッド
	public void userUpdate(String id, String password, String name, String birthDate) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "update user set update_date = NOW()";

			if(!name.equals("")) {
	       		 sql += " , name ='" + name + "'";

	       	 }

	       	 if(!birthDate.equals("")) {
	       		 sql += " , birth_date = '" +birthDate + "'";
	       	 }

	       	 if(!password.equals("")) {
	       		 sql += " , password = '" + password + "'";
	       	 }

	       	 if(!id.equals("")) {
	       		 sql += " where id = '" + id + "'";
	       	 }

			Statement stmt = conn.createStatement();

			stmt.executeUpdate(sql);

			System.out.println(sql);

 		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	//削除メソッド
	public void userDelete (String id) {
    	Connection conn = null;

        try {

            conn = DBManager.getConnection();

            String sql ="delete from user where id = ?";
            PreparedStatement pStmt = conn.prepareStatement(sql);

            pStmt.setString(1, id);

            pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
	//検索メソッド
	public List<User> findUsers(String id, String loginId, String name, String birthDate1, String birthDate2){
		Connection conn = null;
		List<User> userFound = new ArrayList<User>();
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT id, login_id, name, birth_date from user where not id = 1";

       	 if(!loginId.equals("")) {
       		 sql += " AND login_id ='" + loginId + "'";

       	 }

       	 if(!name.equals("")) {
       		 sql += " AND name like '%" + name + "%'";
       	 }

       	 if(!birthDate1.equals("")) {
       		 sql += " AND birth_date >= '" + birthDate1 + "'";
       	 }

       	 if(!birthDate2.equals("")) {
       		 sql += " AND birth_date <= '" + birthDate2 + "'";
       	 }

			Statement stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()) {
            	int idData = rs.getInt("id");
                String loginIdData = rs.getString("login_id");
                String nameData = rs.getString("name");
                Date birthDateData = rs.getDate("birth_date");

                User user = new User(idData, loginIdData, nameData, birthDateData);

                userFound.add(user);
            }

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userFound;
	}
	//暗号化
	public String secret(String password) {
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		return result;
	}
}
